from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='tic-task-toe library',
    version='1.0.0',
    description='a simple application to manage your task plans',
    packages=find_packages(),
    install_requires=['peewee'],
    test_suite='ttt_lib.tests.run_tests'
)