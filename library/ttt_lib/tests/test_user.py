import unittest
import console.command_line_interface.controllers.authorization as auth
import library.ttt_lib.models.components.error as errors
import library.ttt_lib.controllers.db_controller as storage
import library.ttt_lib.controllers.models_controller as models


class TestUserController(unittest.TestCase):
    def setUp(self):
        storage.DB_NAME = ':memory:'
        storage.database_init()

    def test_auth_check(self):
        status = auth.auth_check()
        self.assertEqual(status, False)

    def test_login(self):
        with self.assertRaises(errors.UserNotExistError): 
            auth.login("kek", "pass")
            
    def test_app_user_settings_update(self):
        response = models.app_user_settings_update("TesterNewName", "mail@mail.mail", "kek")
        self.assertEqual(response, "Nothing was changed.")

    def test_user_select(self):
        response = storage.user_select(username="tester3")
        self.assertEqual(response.username, "tester3")

    def test_user_select_invalid(self):
        response = storage.user_select(username="fdasdaasdada")
        self.assertEqual(response, None)


