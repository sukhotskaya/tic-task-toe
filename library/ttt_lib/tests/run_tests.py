import unittest


def run_tests():
    testmodules = [
        'ttt_lib.tests.test_project',
        'ttt_lib.tests.test_task',
        'ttt_lib.tests.test_tasklist',
        'ttt_lib.tests.test_user'
        ]

    suite = unittest.TestSuite()

    for t in testmodules:
        suite.addTests(unittest.defaultTestLoader.loadTestsFromName(t))

    unittest.TextTestRunner().run(suite)


if __name__ == "ttt_lib.tests.run_tests":
    run_tests()
