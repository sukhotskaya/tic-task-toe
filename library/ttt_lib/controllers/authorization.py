import library.ttt_lib.controllers.db_controller as storage
import library.ttt_lib.models.user as UserClass
import library.ttt_lib.models.notification as NotificationClass
import library.ttt_lib.models.components.error as errors

''' Auth Controller:
This module performs basic actions with user authorization.
You can login, register and end a session.

'''


def registration(username, mail, password):
    ''' Creates user account.

    Arguments:
      username -- string value, required for auth (required)
      mail -- string value (required)
      password -- string value (required)

    '''
    user = storage.user_select(username)

    if user:
        raise errors.UserExistError()            
    else:
        new_user = UserClass.User(username, mail, password)
        storage.user_insert(new_user)
        account = storage.user_select(username)
        notification = NotificationClass.Notification(title="Account notification",
                                                      body="Welcome! You just created new account",
                                                      sender=0, receiver=account.ID, priority=2)
        storage.notification_insert(notification)

        return True            
