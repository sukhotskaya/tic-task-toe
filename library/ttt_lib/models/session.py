import datetime

class Session:
    ''' Session class documentation:
    Used for user authorization in application.

    Fields:
       ID -- int value, generated automaticly by database;
       owner -- int value, means authorized user ID value;
       expire -- datetime value, means when this user will automaticly logged out from application.
    '''   
    def __init__(self, owner, ID=None, expire=datetime.datetime.now() + datetime.timedelta(days=1)):
        self.ID = ID
        self.owner = owner
        self.expire = expire