class Relation:
    ''' Relation class documentation:
    Used for fast access to all related items.

    Fields:
       rtype -- string value, means relation type ("project", "tasklist", "task", "subscribers");
       rid -- int value, means parent item ID;
       user -- int value, means related item ID.
    ''' 
    def __init__(self, rtype, rid, user):
        self.rtype = rtype
        self.rid = rid
        self.user = user