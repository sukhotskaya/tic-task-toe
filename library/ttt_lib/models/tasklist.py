import datetime
from library.ttt_lib.models.components.priority_values import PRIORITY_VALUES

class Tasklist:
    ''' Tasklist class documentation:

    Fields:
       ID -- int value, generated automaticly by database. Used for tasklist-managing;
       project -- int value, means tasklists parent project (non-required);
       title -- string value;
       owner -- int value, means ID of owner-user;
       priority -- int value (0: "regular", 1: "regular_colored", 2: "middle", 3: "high", 4: "done");
       creation_time -- datetime value, generated automaticly by database, means datetime of tasklist creation;
       is_deleted -- int value, marks tasklist as archived (0/1).
    '''
    def __init__(self, title, owner, is_deleted=0, creation_time=datetime.datetime.now(), priority=0, project=None, ID=None):
        self.ID = ID
        self.project = project
        self.title = title  
        self.owner = owner  
        self.priority = priority
        self.creation_time = creation_time
        self.is_deleted = is_deleted

    def get_priority_value(self):
        ''' Converts priority value to string.
        '''
        return PRIORITY_VALUES.get(self.priority)