import datetime
import calendar

SHIFT_VALUES = {
    1: datetime.timedelta(days=1),
    3: datetime.timedelta(days=7),
    5: datetime.timedelta(days=calendar.monthrange(datetime.date.today().year, datetime.date.today().month)[1])
}

