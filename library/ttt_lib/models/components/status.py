from enum import Enum


class Status(Enum):
    ONGOING = 0
    DONE = 1

    @staticmethod
    def get_by_number(number):

        return Status(number)
