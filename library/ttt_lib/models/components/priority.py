from enum import Enum


class Priority(Enum):
    LOW = 0
    MEDIUM = 1
    HIGH = 2

    @staticmethod
    def get_by_number(number):

        return Priority(number)
