from library.ttt_lib.models.components.shift_values import SHIFT_VALUES


class Period:
    ''' Period class documentation:
    Used for task deadline time-shifting.

    Fields:
       ID -- int value, generated automaticly by database;
       task -- int value, means task ID;
       shift -- int value, means time-shift period (1..5).
    '''
    def __init__(self, task, shift, ID=None):
        self.ID = ID
        self.task = task
        self.shift = shift

    def get_shift(self):
        ''' Converts int time-shift value to datetime.
        '''
        return SHIFT_VALUES.get(self.shift)
