import datetime


class TaskGroup:
    def __init__(self,
                 unique_id=None,
                 name=None,
                 creator=None,
                 creation_time=datetime.datetime.now(),
                 is_archived=0):
        self.unique_id = unique_id
        self.name = name
        self.creator = creator
        self.creation_time = creation_time
        self.is_archived = is_archived
