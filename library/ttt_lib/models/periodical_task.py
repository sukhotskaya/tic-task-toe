import datetime
import calendar

from library.ttt_lib.models.components import time_deltas

TIME_DELTAS = {
    time_deltas.DAILY_DELTA_CODE: datetime.timedelta(days=1),
    time_deltas.WEEKLY_DELTA_CODE: datetime.timedelta(days=7),
    time_deltas.MONTHLY_DELTA_CODE: datetime.timedelta(days=calendar.monthrange(datetime.date.today().year,
                                                                                datetime.date.today().month)[1])
}


class PeriodicalTask:
    def __init__(self,
                 unique_id=None,
                 task_id=None,
                 delta=None):
        self.unique_id = unique_id
        self.task_id = task_id
        self.delta = delta

    @property
    def get_delta(self):
        return TIME_DELTAS.get(self.delta)
