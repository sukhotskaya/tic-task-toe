class Connection:

    """

    Connection between user and project

    """

    def __init__(self, type, first_id, second_id):
        self.type = type
        self.first_id = first_id
        self.second_id = second_id
