import peewee

database = peewee.SqliteDatabase('ttt.peewee')


class Connection(peewee.Model):
    type = peewee.CharField()
    first_id = peewee.IntegerField()
    second_id = peewee.IntegerField()

    class Meta:
        database = database


class Log(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True),
    name = peewee.CharField(),
    description = peewee.TextField(),
    sender_id = peewee.IntegerField(),
    receiver_id = peewee.IntegerField(),
    is_archived = peewee.IntegerField(),
    creation_time = peewee.DateTimeField(),
    priority = peewee.IntegerField()

    class Meta:
        database = database


class Notification(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True),
    name = peewee.CharField(),
    description = peewee.TextField(),
    sender_id = peewee.IntegerField(),
    receiver_id = peewee.IntegerField(),
    is_archived = peewee.IntegerField(),
    creation_time = peewee.DateTimeField(),
    priority = peewee.IntegerField()

    class Meta:
        database = database


class PeriodicalTask(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True)
    task_id = peewee.IntegerField()
    delta = peewee.IntegerField()

    class Meta:
        database = database


class Session(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True),
    creator = peewee.IntegerField(),
    finish_time = peewee.DateTimeField()

    class Meta:
        database = database


class Task(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True),
    name = peewee.CharField(),
    creator = peewee.IntegerField(),
    is_archived = peewee.IntegerField(),
    deadline = peewee.DateTimeField(null=True),
    edit_time = peewee.DateTimeField(null=True),
    creation_time = peewee.DateTimeField(),
    period = peewee.IntegerField(),
    status_change_time = peewee.DateTimeField(null=True),
    task_type = peewee.IntegerField(),
    tasklist = peewee.IntegerField(null=True),
    description = peewee.TextField(),
    priority = peewee.IntegerField(),
    status = peewee.IntegerField()

    class Meta:
        database = database


class TaskGroup(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True),
    name = peewee.CharField(),
    creator = peewee.IntegerField(),
    creation_time = peewee.DateTimeField(),
    is_archived = peewee.IntegerField()

    class Meta:
        database = database
        

class TaskList(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True),
    name = peewee.CharField(),
    creator = peewee.IntegerField(),
    is_archived = peewee.IntegerField(),
    creation_time = peewee.DateTimeField(),
    following_list = peewee.IntegerField(null=True),
    priority = peewee.IntegerField(),
    taskgroup = peewee.IntegerField(null=True)

    class Meta:
        database = database


class User(peewee.Model):
    unique_id = peewee.AutoField(primary_key=True)
    name = peewee.CharField()
    username = peewee.CharField()
    password = peewee.CharField()
    mail = peewee.CharField()
    creation_time = peewee.DateTimeField()

    class Meta:
        database = database
