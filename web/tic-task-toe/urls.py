from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.urls import path
from . import views

app_name = "tic-task-toe"
urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login, name='login'),
    path('registration', views.registration, name='registration'),
    path('logout', views.logout_view, name='logout'),
    path('projects', views.projects, name='projects'),
    path('project/<int:project_id>', views.project, name='project'),
    path('tasklists/<int:tasklist_id>', views.tasklist, name='tasklist'),
    path('logs/<int:project_id>', views.logs, name='logs'),
]
