from django.apps import AppConfig


class TasqeConfig(AppConfig):
    name = 'tic-task-toe'
