from django.shortcuts import render, redirect
from django.template import Context, loader
from django.contrib import auth
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
# import library.ttt_lib.controllers.classes_controller as classes
import library.ttt_lib.controllers.db_controller as storage
import library.ttt_lib.controllers.models_controller as models
import library.ttt_lib.models.components.error as errors
# import datetime

from library.ttt_lib.controllers.authorization import registration as lib_registration


def index(request):
    if not request.user.is_authenticated:
        return redirect('/login')

    return render(request, 'home.html', {"user": request.user})


def logout_view(request):
    auth.logout(request)
    return redirect('/login')


def login(request):
    if request.method == 'POST':
        username = request.POST.get('usr', '')
        password = request.POST.get('pwd', '')

        error = None
        user = auth.authenticate(username=username, password=password)
        if user:
            auth.login(request, user)
        else:
            error = "Invalid creds."

        if error:
            return render(request, 'login.html', {"error_message": error})

        return redirect('/')
    else:
        return render(request, 'login.html')


def validate_registration(email, username, pass1, pass2):
    error = None
    if pass1 != pass2:
        error = "Passwords are not the same."

    if pass1 == '':
        error = "Password cant be empty."

    return error


def registration(request):
    if request.method == 'POST':
        email = request.POST.get('mail', '')
        username = request.POST.get('username', '')
        pass1 = request.POST.get('password1', '')
        pass2 = request.POST.get('password2', '')
        error = validate_registration(email, username, pass1, pass2)

        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()

            newuser = auth.authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password2'])
            auth.login(request, newuser)
            lib_registration(username, email, pass2)
        else:
            error = str(form.errors)

        if error:
            return render(request, 'registration.html', {"error_message": error})
        return redirect('/')
    else:
        return render(request, 'registration.html')


def projects(request):
    account = storage.user_select(request.user.username)
    try:
        projects = models.app_project_showall((account.ID, account.username))
    except errors.tasQError:
        projects = []

    if request.POST:
        if request.POST.get("create_project", None):
            project_title = request.POST.get("title", "")

            try:
                models.app_project_create(project_title, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'projects.html', {"user": request.user, "projects": projects, "error": e})
        elif request.POST.get("edit_project", None):
            id = request.POST.get("edit_project", None)
            project_title = request.POST.get("title", "")
            try:
                models.app_project_edit(id, project_title, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'projects.html', {"user": request.user, "projects": projects, "error": e})
        elif request.POST.get("delete_project", None):
            id = request.POST.get("delete_project", None)
            try:
                models.app_project_delete(id, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'projects.html', {"user": request.user, "projects": projects, "error": e})
        return redirect(request.path)

    return render(request, 'projects.html', {"user": request.user, "projects": projects})


def project(request, project_id):
    account = storage.user_select(request.user.username)
    project = models.app_project_show(project_id, (account.ID, account.username))
    try:
        tasklists = models.app_tasklists_showall(project_id, (account.ID, account.username))
    except errors.tasQError:
        tasklists = []

    if request.POST:
        if request.POST.get("create_tasklist", None):
            tasklist_title = request.POST.get("title", "")

            try:
                models.app_tasklist_create(tasklist_title, project_id, 0, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'taskgroup.html', {"user": request.user, "tasklists": tasklists, "error": e, "project": project})
        elif request.POST.get("edit_tasklist", None):
            id = request.POST.get("edit_tasklist", None)
            tasklist_title = request.POST.get("title", "")
            tasklist_priority = request.POST.get("priority", "")
            try:
                models.app_tasklists_edit(id, tasklist_title, int(tasklist_priority), (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'taskgroup.html', {"user": request.user, "tasklists": tasklists, "error": e, "project": project})
            except ValueError as e:
                return render(request, 'taskgroup.html', {"user": request.user, "tasklists": tasklists, "error": {"desc": "Priority must be integer!"}, "project": project})
        elif request.POST.get("delete_tasklist", None):
            id = request.POST.get("delete_tasklist", None)
            try:
                models.app_tasklist_delete(id, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'taskgroup.html', {"user": request.user, "tasklists": tasklists, "error": e, "project": project})
        return redirect(request.path)

    return render(request, 'taskgroup.html', {"user": request.user, "tasklists": tasklists, "project": project})


def tasklist(request, tasklist_id):
    account = storage.user_select(request.user.username)
    tasklist_name, tasks = models.app_tasklist_show(tasklist_id, (account.ID, account.username))

    if request.POST:
        if request.POST.get("create_task", None):
            task_title = request.POST.get("title", "")

            try:
                models.app_task_create(task_title, None, 0, tasklist_id, "", 0, None, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'tasklist.html',
                              {"user": request.user, "tasks": tasks, "error": e, "tasklist_name": tasklist_name,
                               "tasklist_id": tasklist_id})
        elif request.POST.get("edit_task", None):
            id = request.POST.get("edit_task", None)
            title = request.POST.get("title", "")
            priority = request.POST.get("priority", "")
            desc = request.POST.get("desc", "")
            period = request.POST.get("period", "")
            try:
                models.app_task_edit(id, title, None, period, tasklist_id, desc, priority, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'tasklist.html',
                              {"user": request.user, "tasks": tasks, "error": e, "tasklist_name": tasklist_name,
                               "tasklist_id": tasklist_id})
            except ValueError as e:
                return render(request, 'tasklist.html',
                              {"user": request.user, "tasks": tasks, "error": {"desc": "Priority and Period must be integer!"}, "tasklist_name": tasklist_name,
                               "tasklist_id": tasklist_id})
        elif request.POST.get("delete_task", None):
            id = request.POST.get("delete_task", None)
            try:
                models.app_task_delete(id, (account.ID, account.username))
            except errors.tasQError as e:
                return render(request, 'tasklist.html',
                              {"user": request.user, "tasks": tasks, "error": e, "tasklist_name": tasklist_name,
                               "tasklist_id": tasklist_id})
        return redirect(request.path)

    return render(request, 'tasklist.html', {"user": request.user, "tasks": tasks, "tasklist_name": tasklist_name, "tasklist_id": tasklist_id})


def logs(request, project_id):
    account = storage.user_select(request.user.username)
    try:
        logs = models.app_log_show_all(project_id, (account.ID, account.username))
    except errors.tasQError as e:
        logs = []

    return render(request, 'logs.html', {"user": request.user, "logs": logs})
