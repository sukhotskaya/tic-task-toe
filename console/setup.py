from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name='tic-task-toe Console',
    packages=find_packages(),
    install_requires=['peewee'],
    entry_points={
        'console_scripts': ['tic-task-toe = command_line_interface.view:start']
    }
)