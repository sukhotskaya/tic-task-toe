import console.command_line_interface.parser as parser


def start():
    parser.start()


if __name__ == "__main__":
    start()